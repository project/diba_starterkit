
## CONTENTS OF THIS FILE

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


## INTRODUCTION

Installation and basic settings for a common Drupal project in Diputació de
Barcelona.

 * For a full description of the module visit
   https://www.drupal.org/project/diba_starterkit

 * To submit bug reports and feature suggestions, or to track changes visit
   https://www.drupal.org/project/issues/diba_starterkit


## REQUIREMENTS

This module requires Drupal 8.x or 9.x core.


## INSTALLATION

To install the Yasm module for Drupal 8, run the following command:
composer require drupal/diba_starterkit
For further information, see:
https://www.drupal.org/docs/develop/using-composer/using-composer-to-manage-drupal-site-dependencies


## CONFIGURATION

No configuration is needed.


## MAINTAINERS

Current maintainers:

 * Oriol Roselló Castells (oriol_e9g) - https://www.drupal.org/u/oriol_e9g

For additional information, see the project page on Drupal.org
<https://www.drupal.org/project/diba_starterkit>
