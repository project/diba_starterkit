<?php

namespace Drupal\diba_starterkit\Services;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\CachedStorage;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Component\Serialization\Yaml;

/**
 * Implements StarterkitManager service.
 */
class StarterkitManager {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\Core\Config\CachedStorage
   */
  protected $configStorage;

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleInstaller;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactory $configFactory,
    CachedStorage $configStorage,
    FileSystemInterface $fileSystem,
    ModuleHandlerInterface $moduleHandler,
    ModuleInstallerInterface $moduleInstaller
  ) {
    $this->configFactory   = $configFactory;
    $this->configStorage   = $configStorage;
    $this->fileSystem      = $fileSystem;
    $this->moduleHandler   = $moduleHandler;
    $this->moduleInstaller = $moduleInstaller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.storage'),
      $container->get('file_system'),
      $container->get('module_handler'),
      $container->get('module_installer')
    );
  }

  /**
   * Get Diba starterkit defaults config files.
   */
  public function getConfigFiles($kit = 'basic') {
    $module_path = $this->moduleHandler->getModule('diba_starterkit')->getPath();
    $path = $this->fileSystem->realpath($module_path . '/config/kits/' . $kit);

    return $this->fileSystem->scanDirectory($path, '/.yml$/', ['key' => 'name']);
  }

  /**
   * Apply Diba starterkit configs.
   *
   * @return array
   *   Lists of settings imported and modules installed/uninstalled.
   */
  public function apply($kit = 'basic') {
    $imported = $installed = $installed_ko = $uninstalled = $uninstalled_ko = [];

    $modules = $this->getModules($kit);

    // Modules to install.
    foreach ($modules['uninstall'] as $module) {
      if ($this->moduleHandler->moduleExists($module)) {
        try {
          $this->moduleInstaller->uninstall([$module]);
          $uninstalled[] = $module;
        }
        catch (\Exception $e) {
          $uninstalled_ko[] = $module;
        }
      }
    }

    // Modules to uninstall.
    foreach ($modules['install'] as $module) {
      if (!$this->moduleHandler->moduleExists($module)) {
        try {
          $this->moduleInstaller->install([$module]);
          $installed[] = $module;
        }
        catch (\Exception $e) {
          $installed_ko[] = $module;
        }
      }
    }

    // Apply configuration files.
    $configs = $this->getConfigFiles($kit);
    // Clear config cache to get real configs.
    $this->configFactory->reset();
    $currentConfigs = $this->configFactory->listAll();

    foreach ($configs as $key => $file) {
      $configuration = Yaml::decode(file_get_contents($file->uri));

      if (in_array($key, $currentConfigs)) {
        // If the config exists override.
        if ($config = $this->configFactory->getEditable($key)) {
          $config->setData($configuration)->save();
          $imported[] = $key;
        }
      }
      else {
        // If the config not exists create.
        $this->configStorage->write($key, $configuration);
      }
    }

    return [
      'installed'      => $installed,
      'installed_ko'   => $installed_ko,
      'uninstalled'    => $uninstalled,
      'uninstalled_ko' => $uninstalled_ko,
      'imported'       => $imported,
    ];
  }

  /**
   * Get modules to install/uninstall byt kit.
   *
   * @return array
   *   List of modules to install/uninstall.
   */
  public function getModules($kit) {
    // Basic settings kit.
    $kit_settings = [
      'install' => [
        'automated_cron',
        'locale',
        'syslog',
      ],
      'uninstall' => [
        'help',
        'tour',
        'shortcut',
        'quickedit',
        'color',
      ],
    ];

    // Invoke HOOK_starterkit_KIT_alter() to attach more settings.
    $this->moduleHandler->alter('starterkit_' . $kit, $kit_settings);

    return $kit_settings;
  }

  /**
   * Get current kits to apply.
   */
  public function getKits() {
    $kits = ['basic' => $this->t('Minimum')];

    // Invoke HOOK_starterkit_info_alter() to attach more kits.
    $this->moduleHandler->alter('starterkit_info', $kits);

    return $kits;
  }

}
