<?php

namespace Drupal\diba_starterkit\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\diba_starterkit\Services\StarterkitManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Diba Starterkit apply settings form.
 */
class ApplySettings extends FormBase {

  /**
   * {@inheritdoc}
   *
   * @var \Drupal\diba_starterkit\Services\StarterkitManager
   */
  protected $starterkitManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    StarterkitManager $starterkitManager
  ) {
    $this->starterkitManager = $starterkitManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('diba_starterkit.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'diba_starterkit_apply_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['help'] = [
      '#markup' => $this->t('If you click "Kit apply" this will override current configuration with kit settings.'),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];
    $form['help2'] = [
      '#markup' => $this->t('To apply not listed kits these should be enabled before in <a href="@link">modules list</a>.', [
        '@link' => '/admin/modules',
      ]),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];

    $kits_info = $this->getKitsInfo();
    foreach ($kits_info as $kit => $kit_info) {
      $modules = $this->starterkitManager->getModules($kit);
      $config_files = $this->starterkitManager->getConfigFiles($kit);

      $info = [
        $this->t('Ensure installed modules: %modules.', ['%modules' => implode(', ', $modules['install'])]),
        $this->t('Ensure uninstalled modules: %modules.', ['%modules' => implode(', ', $modules['uninstall'])]),
        $this->t('Settings files to apply: %settings.', ['%settings' => implode(', ', array_keys($config_files))]),
      ];

      $form['help_' . $kit] = [
        '#type' => 'details',
        '#title' => $this->t('Kit %name info', ['%name' => $kit_info['name']]),
        '#description' => '<ul><li>' . implode('</li><li>', $info) . '</li></ul>',
        '#open' => FALSE,
      ];
    }
    $form['kit'] = [
      '#type' => 'select',
      '#title' => $this->t('Kit to apply'),
      '#options' => $this->starterkitManager->getKits(),
      '#default_value' => 'basic',
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Kit apply'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $kit = $form_state->getValue('kit');
    if ($result = $this->starterkitManager->apply($kit)) {
      // Kit settings.
      if (!empty($result['imported'])) {
        $this->messenger()->addWarning($this->t('Configurations overrided: %imported.', [
          '%imported' => implode(', ', $result['imported']),
        ]));
      }
      else {
        $this->messenger()->addError($this->t('Nothing imported.'));
      }
      // Kit uninstalled modules.
      if (!empty($result['uninstalled'])) {
        $this->messenger()->addWarning($this->t('Modules uninstalled: %modules.', [
          '%modules' => implode(', ', $result['uninstalled']),
        ]));
      }
      if (!empty($result['uninstalled_ko'])) {
        $this->messenger()->addWarning($this->t('This modules cannot be uninstalled because have dependent data: %modules. Uninstall manually.', [
          '%modules' => implode(', ', $result['uninstalled_ko']),
        ]));
      }
      // Kit installed modules.
      if (!empty($result['installed'])) {
        $this->messenger()->addWarning($this->t('Modules installed: %modules.', [
          '%modules' => implode(', ', $result['installed']),
        ]));
      }
      if (!empty($result['installed_ko'])) {
        $this->messenger()->addWarning($this->t('This modules cannot be installed because needs missing dependencies: %modules. Install manually', [
          '%modules' => implode(', ', $result['installed_ko']),
        ]));
      }

      $this->messenger()->addMessage($this->t('Kit %kit applied successfully.', ['%kit' => $kit]));
    }
    else {
      $this->messenger()->addError($this->t('Nothing imported.'));
    }
  }

  /**
   * Kits info array.
   */
  private function getKitsInfo() {
    $kits = $this->starterkitManager->getKits();

    $kits_info = [];
    foreach ($kits as $key => $kit) {
      $kits_info[$key] = [
        'name'     => $kit,
        'settings' => implode(', ', array_keys($this->starterkitManager->getConfigFiles($key))),
        'modules'  => $this->starterkitManager->getModules($key),
      ];
    }

    return $kits_info;
  }

}
