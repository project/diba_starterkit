<?php

namespace Drupal\Tests\diba_starterkit\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides a class for Diba starterkit standard functional tests.
 *
 * @group diba
 */
class DibaStarterkitSubmodulesTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'diba_starterkit',
    'diba_starterkit_core',
    'diba_starterkit_standard',
  ];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Needed for Backup&Migrate module and others.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Admin users with administer configuration permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create user.
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'view the administration theme',
    ]);
  }

  /**
   * Test users access and apply.
   */
  public function testsDibaStarterkitCore() {
    // Tests admin user has settings access.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/development/diba_starterkit');
    $this->assertSession()->statusCodeEquals(200);

    // Ensures that kits are aviable to apply.
    $this->assertSession()->optionExists('edit-kit', 'basic');
    $this->assertSession()->optionExists('edit-kit', 'core');
    $this->assertSession()->optionExists('edit-kit', 'standard');
  }

}
