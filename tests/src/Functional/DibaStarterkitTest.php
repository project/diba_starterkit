<?php

namespace Drupal\Tests\diba_starterkit\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides a class for Diba starterkit functional tests.
 *
 * @group diba
 */
class DibaStarterkitTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user', 'diba_starterkit'];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Needed for Backup&Migrate module and others.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Admin users with administer configuration permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Basic users without administer configuration permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $basicUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create users.
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'view the administration theme',
    ]);
    $this->basicUser = $this->drupalCreateUser([
      'access administration pages',
      'view the administration theme',
    ]);
  }

  /**
   * Test users access and apply.
   */
  public function testsDibaStarterkit() {
    // Basic user not has settings access.
    $this->drupalLogin($this->basicUser);
    $this->drupalGet('admin/config/development/diba_starterkit');
    $this->assertSession()->statusCodeEquals(403);

    // Admin user has settings access.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/development/diba_starterkit');
    $this->assertSession()->statusCodeEquals(200);

    // Ensures that basic kit are aviable to apply.
    $this->assertSession()->optionExists('edit-kit', 'basic');
  }

}
