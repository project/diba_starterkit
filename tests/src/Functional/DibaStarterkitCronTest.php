<?php

namespace Drupal\Tests\diba_starterkit\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides a class for Diba starterkit functional cron tests.
 *
 * @group diba
 */
class DibaStarterkitCronTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['diba_starterkit'];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Needed for Backup&Migrate module and others.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Admin users with administer configuration permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create admin user.
    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'view the administration theme',
      'access content',
    ]);
  }

  /**
   * Test cron runs without errors.
   */
  public function testsCron() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/system/cron');
    $this->submitForm([], 'Run cron');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Cron ran successfully.');
  }

}
